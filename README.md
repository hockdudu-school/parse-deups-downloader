# Deups downloader

## What is it

This is a program for bulk-downloading all [Deups](https://deups.iet-gibb.ch) units. With it you can download all units + words + sentences from Deups and have it offline with you

### Why?

Why not?

Deups got offline once, so I thought, I not downloading everything and then make an app with it?

So, this tool helps you downloading everything and then saving it on `.csv` files.

## Usage

You'll need **Node.js** and **npm**. You can install both [here](https://nodejs.org/en/download/) (Node.js installs npm too)

1. Clone the repository
2. Copy `config.example.json` to `config.json` and configure it to suit your needs
3. Install the dependencies by writing `npm i` on the terminal
4. Run it by executing `node downloader.js` or `npm start`