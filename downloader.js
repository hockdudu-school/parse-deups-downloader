const WebSocket = require('ws');
const fs = require('fs');

let config = fs.readFileSync("config.json");
config = JSON.parse(config.toString());

const preObject = {
    auth: {msg: "connect", version: "1", support: ["1", "pre2", "pre1"]},
    getUnitsList(callId) {
        return {msg: "sub", id: callId, name: "allUnits", params: []}
    },
    unitGet(unitId, callId = "1") {
        return {msg: "method", method: "practiceUnit", "params": [unitId], id: callId}
    }
};

const randomAlphanumerical = () => Math.random().toString(36).slice(2);
const randomNumber = () => Math.floor(Math.random() * 1000);
const getSocketUrl = () => `wss://deups.iet-gibb.ch/sockjs/${randomNumber()}/${randomAlphanumerical()}/websocket`;

let unitsList = [];
let downloadStack = [];

const socketForGettingAllUnits = new WebSocket(getSocketUrl());
socketForGettingAllUnits.addEventListener('open', () => sendRequest(preObject.auth, socketForGettingAllUnits));

socketForGettingAllUnits.addEventListener('message', (event) => {
    const serverMessage = unstringifyResponse(event.data);

    logger(serverMessage);

    if (serverMessage.hasOwnProperty("msg")) {
        switch (serverMessage.msg) {
            case 'connected':
                sendRequest(preObject.getUnitsList('42'), socketForGettingAllUnits);
                break;
            case 'added':
                if (serverMessage.hasOwnProperty('collection') && serverMessage.collection === 'units') {
                    unitSaver(serverMessage);
                    unitsList.push({
                        id: serverMessage.id,
                        name: serverMessage.fields.name
                    });
                }
                break;
            case 'ready':
                socketForGettingAllUnits.close(1000, "");
                downloadSentences();
                break;
        }
    }
});

const downloadSentences = () => {
    for (let unit of unitsList) {
        for (let i = 0; i < config.downloading.repeats; i++) {
            downloadStack.push(unit);
        }
    }
    downloadNext();
};

const downloadNext = () => {
    if (!downloadStack.length) {
        console.log("Done!");
        return;
    }

    const socks = new WebSocket(getSocketUrl());

    const nextDownload = downloadStack.splice(0, 1)[0];
    const unitId = nextDownload.id;

    const totalDownloads = unitsList.length * config.downloading.repeats;
    const currentDownload = totalDownloads - downloadStack.length;
    const percentage = parseFloat(currentDownload / totalDownloads * 100).toFixed(2);

    const currentUnitDownload = (() => {
        const modulo = currentDownload % config.downloading.repeats;
        return modulo ? modulo : config.downloading.repeats;
    })();

    socks.addEventListener('open', () => {
        sendRequest(preObject.auth, socks);
        sendRequest(preObject.unitGet(unitId, unitId), socks);
    });

    socks.addEventListener('message', (event) => {
        const serverMessage = unstringifyResponse(event.data);

        logger(serverMessage);

        if (serverMessage.msg === 'result') {
            dataSaver(serverMessage);

            if (config.downloading.progress) {
                console.log(
                    `Downloaded ${currentDownload} from ${totalDownloads}, ${percentage}% - ` +
                    `${nextDownload.name} (${currentUnitDownload}/${config.downloading.repeats})`);
            }

            socks.close(1000, "");
            downloadNext();
        }
    });
};


const sendRequest = (object, socket) => {
    if (config.logging.enabled && config.logging.client_log)
        console.log(object);

    socket.send(stringifyRequest(object));
};

const unstringifyResponse = (serverMessage) => {
    let msg = serverMessage.replace(/^a/, '');

    try {
        msg = JSON.parse(msg);
        msg = msg[0];
        msg = JSON.parse(msg)
    } catch (e) {
        return msg;
    }

    return msg;
};

const stringifyRequest = (request) => {
    let msg = JSON.stringify(request);
    msg = JSON.stringify(msg);
    msg = msg.replace(/^"/, '').replace(/"$/, '');
    msg = `["${msg}"]`;
    return msg;
};

const unitSaver = (object) => {
    const unitId = object.id;
    const unitName = object.fields.name;

    const savePath = config.saving.folder;
    const saveUnits = `${config.saving.folder}/${config.saving.units}`;

    if (!fs.existsSync(savePath))
        fs.mkdirSync(savePath);

    if (!fs.existsSync(saveUnits)) {
        fs.writeFile(saveUnits, "id,name\n", (err) => {
            if (err) console.log(err)
        });
    }


    fs.appendFile(saveUnits, `${unitId},${unitName}\n`, (err) => {
        if (err) console.log(err);
    })

};

const dataSaver = (object) => {
    const unitId = object.id;
    const savePath = config.saving.folder;
    const saveWords = `${config.saving.folder}/${config.saving.words}`;
    const saveSentences = `${config.saving.folder}/${config.saving.sentences}`;

    if (!fs.existsSync(savePath))
        fs.mkdirSync(savePath);


    if (!fs.existsSync(saveWords)) {
        fs.writeFile(saveWords, "id,word,fk_unit\n", (err) => {
            if (err) console.log(err)
        });
    }

    if (!fs.existsSync(saveSentences)) {
        fs.writeFile(saveSentences, "id,sentence,fk_word,fk_unit\n", (err) => {
            if (err) console.log(err)
        });
    }

    object.result.sentences.forEach((sentence, index) => {
        const linkedWord = object.result.words[index];

        fs.appendFile(saveSentences, `${sentence._id},"${sentence.sentence.replace(/"/g, '""')}",${linkedWord._id},${unitId}\n`, (err) => {
            if (err) console.log(err);
        });

        fs.appendFile(saveWords, `${linkedWord._id},${linkedWord.word},${unitId}\n`, (err) => {
            if (err) console.log(err);
        })
    });
};

const logger = (object) => {

    if (config.debugging.enabled) {
        let output = JSON.stringify(object);
        if (config.debugging.date) output = `[${(new Date()).toISOString()}] ${output}`;
        fs.appendFile(config.debugging.file, `${output}\n`, (err) => {
            if (err) console.log(err);
        });
    }


    if (!config.logging.enabled) return;

    if (!config.logging.only_unknown) {
        console.log(object);
        return;
    }

    if (object.hasOwnProperty("server_id")) return;

    if (!object.hasOwnProperty("msg")) {
        if (object !== "o") {
            console.log(object);
        }
        return;
    }

    switch (object.msg) {
        case "updated":
        case "connected":
        case "ping":
        case "pong":
        case "added":
        case "result":
        case "ready":
            break;
        default:
            console.log(object);
    }
};
